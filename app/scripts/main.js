// Draw styled map
mapboxgl.accessToken = 'pk.eyJ1IjoibWlnYWx1IiwiYSI6ImNpdzk0b3h6bDAwMTMyb3VtaHRjcWp2YXMifQ.g9rEWEjJBFcsHYE52NCVpQ';
var map = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/migalu/ciw9btr0k003y2qnuh70pqm5n', //hosted style id
    center: [13.404954, 52.520007], // starting position
    zoom: 15,
    pitch: 50,
    maxZoom: 17,
    minZoom: 15,
});


// Set user position in real time
window.navigator.geolocation.watchPosition(setUserLocation);
var enabled = true;
var userMarker;
var userPosition;
var userCoords = [];

function setUserLocation (position) {
  if (enabled) {
    map.jumpTo({
      center: [position.coords.longitude, position.coords.latitude],
      zoom: 15,
    });

    var el = document.createElement('div');
    el.className = 'userMarker';
    el.style.backgroundImage = 'url(../images/user.png)';

    userMarker = new mapboxgl.Marker(el, {offset: [-25, -75]})
    .setLngLat([position.coords.longitude, position.coords.latitude])
    .addTo(map);

    enabled = false
  }
  userPosition = position
  userCoords = [position.coords.longitude, position.coords.latitude];

  userMarker.setLngLat([position.coords.longitude, position.coords.latitude]);
}


// Data example
var challenge_list =
  [{
    id: 0,
    name: "Test name 1",
    description: "Test description 1",
    reward: 100,
    step1_text: "Step 1 text",
    step2_text: "Step 2 text",
    step3_text: "Step 3 text",
    step4_text: "Step 4 text",
    step5_text: "Step 5 text",
    step1_coordinates: [13.44152569770813, 52.500927539186456],
    step2_coordinates: null,
    step3_coordinates: null,
    step4_coordinates: null,
    step5_coordinates: null,
    total_steps: 1,
  },
  {
    id: 1,
    name: "Test name 2",
    description: "Test description 2",
    reward: 200,
    step1_text: "Step 1 text",
    step2_text: "Step 2 text",
    step3_text: "Step 3 text",
    step4_text: "Step 4 text",
    step5_text: "Step 5 text",
    step1_coordinates: [13.402923, 52.529931],
    step2_coordinates: [13.402923, 52.529931],
    step3_coordinates: null,
    step4_coordinates: null,
    step5_coordinates: null,
    total_steps: 2,
  },
  {
    id: 2,
    name: "Test name 2",
    description: "Test description 2",
    reward: 200,
    step1_text: "Step 1 text",
    step2_text: "Step 2 text",
    step3_text: "Step 3 text",
    step4_text: "Step 4 text",
    step5_text: "Step 5 text",
    step1_coordinates: [13.432782, 52.499647],
    step2_coordinates: null,
    step3_coordinates: null,
    step4_coordinates: null,
    step5_coordinates: null,
    total_steps: 1,
  },
  {
    id: 3,
    name: "Test name",
    description: "Test description 2",
    reward: 220,
    step1_text: "Step 1 text",
    step2_text: "Step 2 text",
    step3_text: "Step 3 text",
    step4_text: "Step 4 text",
    step5_text: "Step 5 text",
    step1_coordinates: [13.43482, 52.510436],
    step2_coordinates: [13.402923, 52.529931],
    step3_coordinates: null,
    step4_coordinates: null,
    step5_coordinates: null,
    total_steps: 2,
  },
  {
    id: 4,
    name: "Test name",
    description: "Test description 2",
    reward: 420,
    step1_text: "Step 1 text",
    step2_text: "Step 2 text",
    step3_text: "Step 3 text",
    step4_text: "Step 4 text",
    step5_text: "Step 5 text",
    step1_coordinates: [13.43482, 52.510436],
    step2_coordinates: null,
    step3_coordinates: null,
    step4_coordinates: null,
    step5_coordinates: null,
    total_steps: 1,
  }];

var user_cache =
  {
    points: 232,
    completed_challenges_id: [1, 4],
    active_challenge_id: null,
    current_step: 1
  };


// Draw challenge markers
challenge_list.forEach(function (challenge) {
  var challengeDone = user_cache.completed_challenges_id.indexOf(challenge.id) != -1;
  var el = document.createElement('div');
  el.className = 'marker';
  el.style.backgroundImage = challengeDone ? 'url(../images/flag_checked.svg)' : 'url(../images/flag.svg)';

  el.addEventListener('click', function() {
      $('#station-details__view').css('display', 'block')
      $('#map').css('display', 'none')
  });

  // add marker to map
  new mapboxgl.Marker(el, {offset: [-25, -90]})
      .setLngLat(challenge.step1_coordinates)
      .addTo(map);
});


// Output meter distance between 2 coordinates
function distanceBetweenCoordinates(lat1, lon1, lat2, lon2){
  var R = 6378.137; // Radius of earth in KM
  var dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
  var dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
  Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
  Math.sin(dLon/2) * Math.sin(dLon/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c;
  return d * 1000; // meters
}


function checkAccomplishments (position) {
  var realTimeLat = position.coords.latitude
  var realTimeLong = position.coords.longitude
  var currentChallenge = challenge_list[user_cache.active_challenge_id]
  var step_coordinates = currentChallenge['step' + user_cache.current_step + '_coordinates']

  var checkInterval = setInterval(function() {
    var distance = distanceBetweenCoordinates(realTimeLat, realTimeLong, step_coordinates[1], step_coordinates[0]);
    console.log(distance);
    if (distance < 400) {
      if (currentChallenge['total_steps'] == user_cache.current_step) {
        user_cache.completed_challenges_id.push(currentChallenge.id)
        user_cache.current_step = 1
        user_cache.currentChallenge = null
        clearInterval(checkInterval)
        console.log('User Completed Challenge')
        $('#challenge-details__view .challenge-congrats').fadeIn();
        setTimeout(function(){
          $('#station-details__view .button.unactive').css('display', 'block')
          $('#station-details__view .button.active').css('display', 'none')
          $('#challenge-details__view').fadeOut()
          $('#station-details__view').fadeIn()
        }, 3000)
      } else {
        user_cache.current_step += 1
        step_coordinates = currentChallenge['step' + user_cache.current_step + '_coordinates']
        console.log('User Completed Step')
        // TODO add popup
      }
    }
  }, 1000);
}

// Event Handlers
$('#station-details__view .button.active').on('click', function(){
  $('#station-details__view').css('display', 'none')
  $('#challenge-details__view').css('display', 'block')
  user_cache.active_challenge_id = 0
})

$('#challenge-details__view').on('click',function(){checkAccomplishments(userPosition)})

$('#station-details__view .header').on('click', function(){
  $('#map').css('display', 'block')
  $('#station-details__view').css('display', 'none')
})
