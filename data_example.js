challenge_list:
  [{
    id: 1,
    name: "Test name 1",
    description: "Test description 1",
    reward: 100,
    coordinates: [52.529931, 13.402923]
    step1_text: "Step 1 text",
    step2_text: "Step 2 text",
    step3_text: "Step 3 text",
    step4_text: "Step 4 text",
    step5_text: "Step 5 text",
  },
  {
    id: 2,
    name: "Test name 2",
    description: "Test description 2",
    reward: 200,
    coordinates: [52.539043, 13.395327]
    step1_text: "Step 1 text",
    step2_text: "Step 2 text",
    step3_text: "Step 3 text",
    step4_text: "Step 4 text",
    step5_text: "Step 5 text",
  }]

user_cache:
  {
    points: 232,
    completed_challenges_id: [2, 4],
    active_challenge_id: 1,
    step1_at: "2016-12-03T14:33:53.962Z",
    step2_at: null,
    step3_at: null,
    step4_at: null,
    step5_at: null,
  }
